#!/usr/bin/env bash

if [ "x" == "x$1" ]; then
	echo "usage: $0 <basename> <pdfname>"
	exit 1
fi

MDNAME=$1

if [ "x" == "x$2" ]; then
	PDFNAME=$MDNAME
else
	PDFNAME=$2
fi

echo using pandoc to build $PDFNAME.pdf from $MDNAME.md
pandoc 	--from markdown  	\
	--to latex		\
	--template template/template.tex	\
        --out $MDNAME.pdf	\
	--latex-engine pdflatex	\
	$MDNAME.md


