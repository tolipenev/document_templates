#!/bin/bash

apt-get update && sudo apt-get install -y \
    git pandoc \
	texlive-latex-extra texlive-latex-recommended texlive-latex-base \
	linkchecker poppler-utils \
