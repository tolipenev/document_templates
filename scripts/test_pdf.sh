#!/usr/bin/env bash

if [ "x" == "x$1" ]; then
	echo "usage: $0 <pdf basename>"
	exit 1
fi

PDFNAME=$1

if ! pdftohtml $PDFNAME.pdf; then
	echo "Failure during conversion"
	exit 2
fi

if ! linkchecker $PDFNAME.html; then
	echo "Failure during linkcheck"
	exit 3
fi
