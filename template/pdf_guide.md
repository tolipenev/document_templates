author:            Anatoli Penev
summary:           How to use PDF builder
id:                pdf_guide
categories:        static page
environments:      markdown
status:            final
feedback link:     https://gitlab.com/moozer/document_templates/issues
analytics account: 0

# How to build PDFs

## Overview of the tutorial

Duration: 1:00

This tutorial shows you how to build PDFs from markdown and have them online. <br>This how the file looks before its converted to PDF:<br>
![Before convert](md_before_convert.png)
<br>This is how the file will look after the conversion:<br>
![After convert_frontpage](front_page_pdf.png)<br>
![After convert_inside](pdf_after_convert.png)

## Needed software

Duration: 10:00

In order to build the pdfs you need some additional software if not already present in your laptop/PC. There is a script that you can execute inside the /scripts folder or copy paste from here in your terminal.

```bash
apt-get update
apt-get install git pandoc texlive-latex-extra texlive-latex-recommended texlive-latex-base
apt-get install linkchecker poppler-utils
```

## Building PDFs

Duration: 10:00

After that just clone the repo and you are ready to start making some beautiful pdfs. You can clone the repo by copy/pasting this command: <br>

```git
git clone https://gitlab.com/moozer/document_templates.git
```

To create a pdf you have to put the .md file in the *tests* folder and run the command while inside the folder:

*template/scripts/build_pdf.sh* **name_of_file** (no need to specify file extension)

To test what you have created use:

*template/scripts/test_pdf.sh* **name_of_file** (no need to specify file extension)

## GitLab CI

Duration: 5:00

This project uses the GitLab CI to generate the example pdfs.
We are using docker image: Debian Stretch as it gives more stable performance. <br>
Here is the yaml file: <br>

```yaml
# build using custom docker
build_pdfs_custom:
  image: registry.gitlab.com/moozer/docker-pdf/stretch:latest
  script:
    - cd tests
    - template/scripts/build_pdf.sh test
    - template/scripts/test_pdf.sh test
    - cd ..
    - mv tests/test.pdf test_custom.pdf
  artifacts:
    paths:
      - test_custom.pdf

pages:
  stage: deploy
  before_script:
  - wget https://github.com/googlecodelabs/tools/releases/download/v2.0.2/claat-linux-amd64
  - chmod +x claat-linux-amd64
  script:
  - ./claat-linux-amd64 export template/pdf_guide.md
  - mkdir -p public
  - cp pages/* public
  - cp pdf_guide public -r
  - cp -r test_custom.pdf public
  artifacts:
    when:
      always
    paths:
    - public
  dependencies:
    - build_pdfs_custom
  only:
    - master
```